Welcome to LoserPay App
   
   Here, a user competes against another user.
   The Winner gets it all.
   The Loser gets nothing.
   Its like a betting app but instead, mortals compete against mortals.

  * DB - PostgreSql.
  * Language - Ruby.
  * Framework - Rails.
  * App type - API.