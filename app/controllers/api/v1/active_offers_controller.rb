class Api::V1::ActiveOffersController < ApplicationController
  before_action :set_active_offer, only: [:show, :update, :destroy]

  def index
    @active_offers = ActiveOffer.all

    render json: @active_offers
  end

  def show
    render json: @active_offer
  end

  def create
    @active_offer = ActiveOffer.new(active_offer_params)

    if @active_offer.save
      render json: @active_offer, status: :created, location: @active_offer
    else
      render json: @active_offer.errors, status: :unprocessable_entity
    end
  end

  def update
    if @active_offer.update(active_offer_params)
      render json: @active_offer
    else
      render json: @active_offer.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @active_offer.destroy
  end

  private
    def set_active_offer
      @active_offer = ActiveOffer.find(params[:id])
    end

    def active_offer_params
      params.require(:active_offer).permit(:final_profit, :offer_num)
    end
end
