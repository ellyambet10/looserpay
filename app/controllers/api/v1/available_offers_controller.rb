class Api::V1::AvailableOffersController < ApplicationController
  before_action :set_available_offer, only: [:show, :update, :destroy]

  def index
    @available_offers = AvailableOffer.all

    render json: available_offers
  end

  def show
    render json: @available_offer
  end

  def create
    @available_offer = AvailableOffer.new(available_offer_params)

    if @available_offer.save
      render json: @available_offer, status: :created, location: @available_offer
    else
      render json: @available_offer.errors, status: :unprocessable_entity
    end
  end

  def update
    if @available_offer.update(available_offer_params)
      render json: @available_offer
    else
      render json: @available_offer.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @available_offer.destroy
  end

  private
    def set_available_offer
      @available_offer = AvailableOffer.find(params[:id])
    end

    def available_offer_params
      params.require(:available_offer).permit(:other_team, :other_stake, :other_profit, :offer_num)
    end
end
