class Api::V1::BalancesController < ApplicationController
  before_action :set_balance, only: [:show, :update, :destroy]

  def index
    @balances = Balance.all

    render json: @balances
  end

  def show
    render json: @balance
  end

  def create
    @balance = Balance.new(balance_params)

    if @balance.save
      render json: @balance, status: :created, location: @balance
    else
      render json: @balance.errors, status: :unprocessable_entity
    end
  end

  def update
    if @balance.update(balance_params)
      render json: @balance
    else
      render json: @balance.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @balance.destroy
  end

  private
    def set_balance
      @balance = Balance.find(params[:id])
    end

    def balance_params
      params.require(:balance).permit(:amount)
    end
end
