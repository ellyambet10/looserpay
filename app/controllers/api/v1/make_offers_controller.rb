class Api::V1::MakeOffersController < ApplicationController
  before_action :set_make_offer, only: [:show, :update, :destroy]

  def index
    @make_offers = MakeOffer.all

    render json: @make_offers
  end

  def show
    render json: @make_offer
  end

  def create
    @make_offer = MakeOffer.new(make_offer_params)

    if @make_offer.save
      render json: @make_offer, status: :created, location: @make_offer
    else
      render json: @make_offer.errors, status: :unprocessable_entity
    end
  end

  def update
    if @make_offer.update(make_offer_params)
      render json: @make_offer
    else
      render json: @make_offer.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @make_offer.destroy
  end

  private
    def set_make_offer
      @make_offer = MakeOffer.find(params[:id])
    end

    def make_offer_params
      params.require(:make_offer).permit(:my_team, :stake, :profit, :offer_num)
    end
end
