class Api::V1::TakeOffersController < ApplicationController
  before_action :set_take_offer, only: [:show, :update, :destroy]

  def index
    @take_offers = TakeOffer.all

    render json: @take_offers
  end

  def show
    render json: @take_offer
  end

  def create
    @take_offer = TakeOffer.new(take_offer_params)

    if @take_offer.save
      render json: @take_offer, status: :created, location: @take_offer
    else
      render json: @take_offer.errors, status: :unprocessable_entity
    end
  end

  def update
    if @take_offer.update(take_offer_params)
      render json: @take_offer
    else
      render json: @take_offer.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @take_offer.destroy
  end

  private
    def set_take_offer
      @take_offer = TakeOffer.find(params[:id])
    end

    def take_offer_params
      params.require(:take_offer).permit(:offer_num)
    end
end
