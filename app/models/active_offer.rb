class ActiveOffer < ApplicationRecord
  belongs_to :take_offer
  belongs_to :creator

  has_many :available_offer, dependent: :destroy

  validates :final_profit, presence: true
  validates :offer_num, uniqueness: true, presence: true
end
