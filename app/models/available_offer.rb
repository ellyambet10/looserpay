class AvailableOffer < ApplicationRecord
  belongs_to :make_offer
  belongs_to :creator
  belongs_to :assignee

  has_many :make_offer, dependent: :destroy

  validates :other_team, presence: true
  validates :other_stake, presence: true
  validates :other_profit, presence: true
  validates :offer_num, uniqueness: true, presence: true
end
