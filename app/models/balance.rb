class Balance < ApplicationRecord
  belongs_to :active_offer
  belongs_to :make_offer
  belongs_to :available_offer
  belongs_to :take_offer
  belongs_to :creator

  validates :amount, presence: true
end
