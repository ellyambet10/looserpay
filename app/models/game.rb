class Game < ApplicationRecord
  belongs_to :creator

  validates :my_team, presence: true
  validates :other_team, presence: true
  validates :my_team_odds, presence: true
  validates :other_team_odds, presence: true
  validates :my_stake, presence: true
  validates :my_profit, presence: true
  validates :min_stake, presence: true

=begin  def play_game
    # team_x Vs team_y
    min_stake = 100
    if team_x == gets.chomp
      team_y = other_team
    else
      team_y = my_team
    end

    my_team_odds * min_stake = my_stake
    other_team_odds * min_stake = my_profit
  end=end
end
