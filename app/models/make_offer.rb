class MakeOffer < ApplicationRecord
  belongs_to :game
  belongs_to :creator

  validates :my_team, presence: true
  validates :stake, presence: true
  validates :profit, presence: true
  validates :offer_num, uniqueness: true, presence: true
end

