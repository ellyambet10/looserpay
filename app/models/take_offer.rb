class TakeOffer < ApplicationRecord
  belongs_to :available_offer
  belongs_to :creator
  belongs_to :assignee

  validates :offer_num, uniqueness: true, presence: true
end
