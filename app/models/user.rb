class User < ApplicationRecord
  include Gravatar

  has_secure_password

  has_many :games, foreign_key: 'creator_id', dependent: :nullify
  has_many :make_offers, foreign_key: 'creator_id', dependent: :nullify
  has_many :available_offers, foreign_key: 'assignee_id', dependent: :nullify
  has_many :take_offers, foreign_key: 'assignee_id', dependent: :nullify
  has_many :active_offers, foreign_key: 'creator_id', dependent: :nullify

  validates :phone_number, uniqueness: true, presence: true

  default_scope { order(:created_at) }

  def self.admin
    where(admin: true)
  end
end
