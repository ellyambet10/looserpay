Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users
      resources :games
      resources :balances
      resources :take_offers
      resources :make_offers
      resources :active_offers
      resources :available_offers
    end
  end
  root 'welcome#index'
end



