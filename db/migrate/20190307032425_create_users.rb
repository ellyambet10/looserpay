class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :phone_number
      t.string :password_digest
      t.boolean :admin, default: false, null: false
      t.datetime :last_login

      t.timestamps
    end
  end
end
