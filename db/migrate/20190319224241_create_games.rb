class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.references :creator, index: true, foreign_key: {to_table: :users}
      t.string :my_team
      t.string :other_team
      t.string :my_team_odds
      t.string :other_team_odds
      t.string :my_stake
      t.string :my_profit
      t.string :min_stake

      t.timestamps
    end
  end
end
