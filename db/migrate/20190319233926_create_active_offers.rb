class CreateActiveOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :active_offers do |t|
      t.references :take_offer, foreign_key: {to_table: :users}
      t.references :creator, index: true, foreign_key: {to_table: :users}
      t.string :offer_num
      t.string :final_profit

      t.timestamps
    end
  end
end
