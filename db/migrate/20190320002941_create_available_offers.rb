class CreateAvailableOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :available_offers do |t|
      t.references :make_offer, foreign_key: {to_table: :users}
      t.references :creator, index: true, foreign_key: {to_table: :users}
      t.references :assignee, foreign_key: {to_table: :users}
      t.string :other_team
      t.string :other_stake
      t.string :other_profit
      t.string :offer_num

      t.timestamps
    end
  end
end
