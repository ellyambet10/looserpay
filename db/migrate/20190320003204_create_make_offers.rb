class CreateMakeOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :make_offers do |t|
      t.references :game, foreign_key: {to_table: :users}
      t.references :creator, index: true, foreign_key: {to_table: :users}
      t.string :my_team
      t.string :stake
      t.string :profit
      t.string :offer_num
      t.boolean :confirm, default: false, null: false

      t.timestamps
    end
  end
end
