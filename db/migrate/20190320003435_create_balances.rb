class CreateBalances < ActiveRecord::Migration[5.2]
  def change
    create_table :balances do |t|
      t.references :active_offer, foreign_key: {to_table: :users}
      t.references :make_offer, foreign_key: {to_table: :users}
      t.references :available_offer, foreign_key: {to_table: :users}
      t.references :take_offer, foreign_key: {to_table: :users}
      t.references :creator, index: true, foreign_key: {to_table: :users}
      t.string :amount

      t.timestamps
    end
  end
end
