class CreateTakeOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :take_offers do |t|
      t.references :available_offer, foreign_key: {to_table: :users}
      t.references :creator, index: true, foreign_key: {to_table: :users}
      t.references :assignee, foreign_key: {to_table: :users}
      t.string :offer_num
      t.boolean :confirm, default: false, null: false

      t.timestamps
    end
  end
end
