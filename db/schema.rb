# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_20_003557) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_offers", force: :cascade do |t|
    t.bigint "take_offer_id"
    t.bigint "creator_id"
    t.string "offer_num"
    t.string "final_profit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_active_offers_on_creator_id"
    t.index ["take_offer_id"], name: "index_active_offers_on_take_offer_id"
  end

  create_table "available_offers", force: :cascade do |t|
    t.bigint "make_offer_id"
    t.bigint "creator_id"
    t.bigint "assignee_id"
    t.string "other_team"
    t.string "other_stake"
    t.string "other_profit"
    t.string "offer_num"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assignee_id"], name: "index_available_offers_on_assignee_id"
    t.index ["creator_id"], name: "index_available_offers_on_creator_id"
    t.index ["make_offer_id"], name: "index_available_offers_on_make_offer_id"
  end

  create_table "balances", force: :cascade do |t|
    t.bigint "active_offer_id"
    t.bigint "make_offer_id"
    t.bigint "available_offer_id"
    t.bigint "take_offer_id"
    t.bigint "creator_id"
    t.string "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["active_offer_id"], name: "index_balances_on_active_offer_id"
    t.index ["available_offer_id"], name: "index_balances_on_available_offer_id"
    t.index ["creator_id"], name: "index_balances_on_creator_id"
    t.index ["make_offer_id"], name: "index_balances_on_make_offer_id"
    t.index ["take_offer_id"], name: "index_balances_on_take_offer_id"
  end

  create_table "games", force: :cascade do |t|
    t.bigint "creator_id"
    t.string "my_team"
    t.string "other_team"
    t.string "my_team_odds"
    t.string "other_team_odds"
    t.string "my_stake"
    t.string "my_profit"
    t.string "min_stake"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_games_on_creator_id"
  end

  create_table "make_offers", force: :cascade do |t|
    t.bigint "game_id"
    t.bigint "creator_id"
    t.string "my_team"
    t.string "stake"
    t.string "profit"
    t.string "offer_num"
    t.boolean "confirm", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_make_offers_on_creator_id"
    t.index ["game_id"], name: "index_make_offers_on_game_id"
  end

  create_table "take_offers", force: :cascade do |t|
    t.bigint "available_offer_id"
    t.bigint "creator_id"
    t.bigint "assignee_id"
    t.string "offer_num"
    t.boolean "confirm", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assignee_id"], name: "index_take_offers_on_assignee_id"
    t.index ["available_offer_id"], name: "index_take_offers_on_available_offer_id"
    t.index ["creator_id"], name: "index_take_offers_on_creator_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "phone_number"
    t.string "password_digest"
    t.boolean "admin", default: false, null: false
    t.datetime "last_login"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "active_offers", "users", column: "creator_id"
  add_foreign_key "active_offers", "users", column: "take_offer_id"
  add_foreign_key "available_offers", "users", column: "assignee_id"
  add_foreign_key "available_offers", "users", column: "creator_id"
  add_foreign_key "available_offers", "users", column: "make_offer_id"
  add_foreign_key "balances", "users", column: "active_offer_id"
  add_foreign_key "balances", "users", column: "available_offer_id"
  add_foreign_key "balances", "users", column: "creator_id"
  add_foreign_key "balances", "users", column: "make_offer_id"
  add_foreign_key "balances", "users", column: "take_offer_id"
  add_foreign_key "games", "users", column: "creator_id"
  add_foreign_key "make_offers", "users", column: "creator_id"
  add_foreign_key "make_offers", "users", column: "game_id"
  add_foreign_key "take_offers", "users", column: "assignee_id"
  add_foreign_key "take_offers", "users", column: "available_offer_id"
  add_foreign_key "take_offers", "users", column: "creator_id"
end
