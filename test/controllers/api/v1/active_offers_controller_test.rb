require 'test_helper'

class Api::V1::ActiveOffersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @active_offer = active_offers(:active_user_1)
  end

  test "should get index" do
    get api_v1_active_offers_url, as: :json
    assert_response :success
  end

  test "should create active_offer" do
    assert_difference('ActiveOffer.count') do
      post api_v1_active_offers_url, params: { active_offer: {offer_num: '1', final_profit: '200' } }, as: :json
    end

    assert_response 201
  end

  test "should show active_offer" do
    get api_v1_active_offer_url(@active_offer), as: :json
    assert_response :success
  end

  test "should update active_offer" do
    patch api_v1_active_offer_url(@active_offer), params: { active_offer: {offer_num: '1', final_profit: '200' } }, as: :json
    assert_response 200
  end

  test "should destroy active_offer" do
    assert_difference('ActiveOffer.count', -1) do
      delete api_v1_active_offer_url(@active_offer), as: :json
    end

    assert_response 204
  end
end
