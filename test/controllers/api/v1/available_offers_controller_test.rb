require 'test_helper'

class Api::V1::AvailableOffersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @available_offer = available_offers(:available_1)
  end

  test "should get index" do
    get api_v1_available_offers_url, as: :json
    assert_response :success
  end

  test "should create available_offer" do
    assert_difference('AvailableOffer.count') do
      post api_v1_available_offers_url, params: { available_offer: {other_team: 'Y', other_stake: '100', other_profit: '200', offer_num: '1' } }, as: :json
    end

    assert_response 201
  end

  test "should show available_offer" do
    get api_v1_available_offer_url(@available_offer), as: :json
    assert_response :success
  end

  test "should update available_offer" do
    patch api_v1_available_offer_url(@available_offer), params: { available_offer: {other_team: 'Y', other_stake: '100', other_profit: '200', offer_num: '1' } }, as: :json
    assert_response 200
  end

  test "should destroy available_offer" do
    assert_difference('AvailableOffer.count', -1) do
      delete api_v1_available_offer_url(@available_offer), as: :json
    end

    assert_response 204
  end
end
