require 'test_helper'

class Api::V1::BalancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @balance = balances(:balance_1)
  end

  test "should get index" do
    get api_v1_balances_url, as: :json
    assert_response :success
  end

  test "should create balance" do
    assert_difference('Balance.count') do
      post api_v1_balances_url, params: { balance: {amount: '500'} }, as: :json
    end

    assert_response 201
  end

  test "should show balance" do
    get api_v1_balance_url(@balance), as: :json
    assert_response :success
  end

  test "should update balance" do
    patch api_v1_balance_url(balance), params: { balance: {amount: '500'} }, as: :json
    assert_response 200
  end

  test "should destroy balance" do
    assert_difference('Balance.count', -1) do
      delete api_v1_balance_url(@balance), as: :json
    end

    assert_response 204
  end
end
