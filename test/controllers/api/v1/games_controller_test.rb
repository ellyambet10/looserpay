require 'test_helper'

class Api::V1::GamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @game = games(:game_1)
  end

  test "should get index" do
    get api_v1_games_url, as: :json
    assert_response :success
  end

  test "should create game" do
    assert_difference('Game.count') do
      post api_v1_games_url, params: { game: {my_team: 'X', other_team: 'Y', my_team_odds: '2', other_team_odds: '1', my_stake: '200', my_profit: '100', min_stake: '100' } }, as: :json
    end

    assert_response 201
  end

  test "should show game" do
    get api_v1_game_url(@game), as: :json
    assert_response :success
  end

  test "should update game" do
    patch api_v1_game_url(@game), params: { game: {my_team: 'X', other_team: 'Y', my_team_odds: '2', other_team_odds: '1', my_stake: '200', my_profit: '100', min_stake: '100' } }, as: :json
    assert_response 200
  end

  test "should destroy game" do
    assert_difference('Game.count', -1) do
      delete api_v1_game_url(@game), as: :json
    end

    assert_response 204
  end
end
