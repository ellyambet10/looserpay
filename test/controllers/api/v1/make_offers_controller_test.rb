require 'test_helper'

class Api::V1::MakeOffersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @make_offer = make_offers(:make_1)
  end

  test "should get index" do
    get api_v1_make_offers_url, as: :json
    assert_response :success
  end

  test "should create make_offer" do
    assert_difference('MakeOffer.count') do
      post api_v1_make_offers_url, params: { make_offer: {my_team: 'X', stake: '200', profit: '100', offer_num: '1' } }, as: :json
    end

    assert_response 201
  end

  test "should show make_offer" do
    get api_v1_make_offer_url(@make_offer), as: :json
    assert_response :success
  end

  test "should update make_offer" do
    patch api_v1_make_offer_url(@make_offer), params: { make_offer: {my_team: 'X', stake: '200', profit: '100', offer_num: '1' } }, as: :json
    assert_response 200
  end

  test "should destroy make_offer" do
    assert_difference('MakeOffer.count', -1) do
      delete api_v1_make_offer_url(@make_offer), as: :json
    end

    assert_response 204
  end
end
