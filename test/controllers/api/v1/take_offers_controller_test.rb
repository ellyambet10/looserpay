require 'test_helper'

class Api::V1::TakeOffersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @take_offer = take_offers(:take_1)
  end

  test "should get index" do
    get api_v1_take_offers_url, as: :json
    assert_response :success
  end

  test "should create take_offer" do
    assert_difference('TakeOffer.count') do
      post api_v1_take_offers_url, params: { take_offer: {offer_num: '1' } }, as: :json
    end

    assert_response 201
  end

  test "should show take_offer" do
    get api_v1_take_offer_url(@take_offer), as: :json
    assert_response :success
  end

  test "should update take_offer" do
    patch api_v1_take_offer_url(@take_offer), params: { take_offer: {offer_num: '1' } }, as: :json
    assert_response 200
  end

  test "should destroy take_offer" do
    assert_difference('TakeOffer.count', -1) do
      delete api_v1_take_offer_url(@take_offer), as: :json
    end

    assert_response 204
  end
end
